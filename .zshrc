export PATH="/usr/local/opt/python@3.8/bin:$PATH"
. /usr/local/share/chruby/chruby.sh
. /usr/local/share/chruby/auto.sh
chruby ruby-3.1.0

function pyvenv () {
    local err=""
    local ret=0
    if [ -z "$1" ]; then
        echo "Usage: pyvenv <venv directory path>";
        return 1;
    fi
    $err=$(python3 -m venv $1 2>&1)
    $ret=$?
    if [ $ret -ne 0 ]; then
        echo "Python virtual environment creation failed";
        echo "$err";
        return $ret;
    fi
    $err=$(. $1/bin/activate 2>&1)
    $ret=$?
    if [ $ret -ne 0 ]; then
        echo "Activation of virtual environment failed";
        echo "$err";
        return $ret;
    fi
    $err=$(pip install --upgrade pip 2>&1)
    $ret=$?
    if [ $ret -ne 0 ]; then
        echo "Updating pip failed";
        echo "$err";
        return $ret;
    fi
    return 0;
}
