# Installation

Either clone the repository normally:

```bash
git clone https://gitlab.com/gigous/configs
```

or go all out:

```bash
mv ~/.vimrc ~/.vimrc.backup
mv ~/.tmux.conf ~/.tmux.conf.backup
git clone https://gitlab.com/gigous/configs ~
```

Then you can commit and pull straight from the home directory, assuming you
don't already have a repo there.
