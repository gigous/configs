" =================== Vundle stuff ======================
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Tmux navigator for Ctrl+direction goodness
Plugin 'christoomey/vim-tmux-navigator'

" Paired surrounding
" cs"' to surround "this" in single quotes like 'this'
Plugin 'tpope/vim-surround'

" toggle comments with gc$count
Plugin 'tpope/vim-commentary'

" A Vim plugin which shows a git diff in the gutter (sign column) and
" stages/undoes hunks.
Plugin 'airblade/vim-gitgutter'

" A Vim alignment plugin
Plugin 'junegunn/vim-easy-align'

" Better whitespace highlighting for Vim
Plugin 'ntpeters/vim-better-whitespace'

" Alternate Files quickly (.c --> .h etc)
Plugin 'vim-scripts/a.vim'

" A code-completion engine for Vim
" TODO add this back but make it not suck (shortcut to enable/disable it?)
"Plugin 'Valloric/YouCompleteMe'

" All of your Plugins must be added before the following line
try
    call vundle#end()            " required
catch
    echo "Setting up Vundle, please wait a moment..."
    !git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
    echo "Vundle has been set up! :D\nGetting other plugins..."
    "TODO fix, doesn't work because it somehow doesn't know what Vundle is yet
    "PluginInstall
    "echo "Yay! Plugins are here... I think?"
endtry
filetype plugin indent on    " required

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" =================== my .vimrc stuff ===================

" TODO Organize better
" TODO Make function that can open cpps and hpps for files side by side

" TODO Fix the YouCompleteMe error message thing lol
" let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/.ycm_extra_conf.py'

" define group so that autocmd's are only applied once
augroup configgroup
    " clear all autocmd for current group
    autocmd!
    " Default options for all filetypes
    autocmd FileType * setlocal shiftwidth=4
    autocmd FileType * setlocal tabstop=4
    autocmd FileType * setlocal backspace=2
    autocmd FileType * setlocal number
    autocmd FileType * setlocal nobackup
    autocmd FileType * setlocal nohlsearch
    autocmd FileType * setlocal smartcase
    autocmd FileType * setlocal ignorecase
    autocmd FileType * setlocal autoindent
    autocmd FileType * setlocal expandtab
    autocmd FileType * setlocal ruler
    autocmd FileType * setlocal updatetime=100  " For gitgutter
    autocmd FileType * setlocal listchars=tab:>-,extends:>,precedes:< " for showing tabs
    autocmd FileType * setlocal list
    autocmd FileType * set colorcolumn=80
    autocmd FileType * hi ColorColumn ctermbg=darkgrey

    """""""""""""""""""""""""""""""""
    " Language-Specific Options     "    btw can do setlocal option1=1 option2 option3
    """""""""""""""""""""""""""""""""    and stuff
    " C: 3 spaces
    autocmd FileType c setlocal shiftwidth=3 tabstop=3 expandtab
    autocmd FileType h setlocal shiftwidth=3 tabstop=3 expandtab
    " C#: 3 spaces, keep tabs
    autocmd FileType cs setlocal shiftwidth=3 tabstop=3 noexpandtab
    " Cfg: 3 spaces, keep tabs
    autocmd FileType cfg setlocal shiftwidth=3 tabstop=3 noexpandtab
    " C++: 3 spaces
    autocmd FileType cpp setlocal shiftwidth=3 tabstop=3 expandtab
    " XML: 2 spaces
    autocmd FileType xml setlocal shiftwidth=2 tabstop=2 expandtab
    " Makefiles, hard tabs
    autocmd FileType make setlocal shiftwidth=4 tabstop=4 noexpandtab
    " TODO What about make.rules and such?
    " Python, 4 spaces
    autocmd FileType py setlocal shiftwidth=4 tabstop=4 expandtab
augroup END


" So that O or o doesn't take forever
set ttimeoutlen=100
syntax on

" :o
" "Variable types, once assigned, are permanent and strictly enforced at runtime"


" You can also read and set registers as variables. Run the following command:
" :let @a = "hello!"


" Use ctrl-[hjkl] to select the active split!
nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-l> :wincmd l<CR>

" Use ctrl-s to split the view horizontally, or ctrl-v to split vertically
" (TODO: ctrl-s might not work in terminal!)
"nmap <silent> <c-s> :wincmd s<CR>
"nmap <silent> <c-v> :wincmd v<CR>

" Adjust size of windows (correct word?) in splits
"nmap <silent> <F2> :wincmd -<CR>
"nmap <silent> <F3> :wincmd +<CR>
"nmap <silent> <F4> :wincmd <<CR>
"nmap <silent> <F5> :wincmd ><CR>

" gb jumps to first tab
nnoremap gb :tabfirst<CR>
" Opens source/header file of current file in vert split (requires a.vim)
nnoremap gav :AV<CR>
" Opens source/header file of current file in split (requires a.vim)
nnoremap gas :AS<CR>
" Toggle text wrapping
nnoremap gw :setlocal wrap!<CR>:setlocal wrap?<CR>
" Toggle highlight for searches
nnoremap gh :setlocal hlsearch!<CR>:setlocal hlsearch?<CR>

" Ctrl+C in visual mode to copy the selected text
" make sure has('clipboard') returns 1
vmap <silent> <c-c> "+y

" Initializer listify/unlistify
vnoremap gl :s/\v\s*\=\s*(.[^;]*);/(\1),/gc<CR>a
vnoremap gL :s/\v\s*\((.*)\)\s*,?\s*/ = \1;/gc<CR>a
" To search for visually selected text, put this line in your vimrc:
vnoremap vnoremap // y/\V<C-R>"<CR>

" TODO: Figure out how to make F2 paste stuff from clipboard pretty-ly
"nmap <F2> :set paste<CR>:r !pbpaste<CR>:set nopaste<CR>
"imap <F2> <Esc>:set paste<CR>:r !pbpaste<CR>:set nopaste<CR>

" Ctrl+[ toggles paste mode thing
nmap <c-p> :set paste!<CR>:set paste?<CR>

" TODO Fix this lol
function! OpenVimrcBuf(vimrclocation)
    let vbufnum = bufwinnr(a:vimrclocation)
    if vbufnum > 0
        " switch to buffer
        echo "vimrc file already open!"
        execute ':b'. vbufnum
    else
        execute ':tabe'. a:vimrclocation
    endif
endfunction

let home = $HOME
if has("win32") || has("win16") " If on Windows (yuck)
    let vimrclocation = home.'\_vimrc' 
else
    let vimrclocation = home.'/.vimrc' " else, probably Linux
endif

" Insert opens this vimrc file for editing
nmap <ins> :call OpenVimrcBuf(vimrclocation)<CR>

" Ctrl+2 to source this vimrc file
map <c-@> :execute 'source 'vimrclocation<CR> :echo 'vimrc sourced!'<CR>

" TODO: When special key combo is pressed and in visual mode, run selected
"       text through vim as a vimscript using yank->:@" or similar 
"       (or pass it to something else)

" Until we can find a better one
colorscheme torte



" TODO TODO TODO Make function/script that saves this vimrc to
" dropbox/somewhere
" Or just git the stuff
